# bot.py
import os

import discord
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')

client = discord.Client()


@client.event
async def on_ready():
    guild = discord.utils.get(client.guilds, name=GUILD)
    print(
        f'{client.user} is connected to the following guild:\n'
        f'{guild.name}(id: {guild.id})'
    )


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if client.user.mentioned_in(message) and message.mention_everyone is False:
        await message.channel.send("What the fuck do you want?")
    if "walmart" in message.content:
        await message.channel.send("i said that")
    if "WALMART" in message.content:
        await message.channel.send("I SAID THAT")
    if "if you hard" in message.content:
        await message.channel.send("you hard")
    if "69" in message.content:
        await message.channel.send("nice")
    if "rude" in message.content:
        await message.channel.send("bitch")

client.run(TOKEN)
